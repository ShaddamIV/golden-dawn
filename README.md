SWGReborn
===================
**Note:  This repository is still a work in progress, as is this README.  If you run into any issues while setting up your server or environment, please let us know on our website ([link](https://swgreborn.com/forums/)).**

This repository houses the source code to the actual game server and other various server applications for SWG as well as the source code to the game files (such as the client and server game data).

**Environment Setup Guide**

 1. Install Visual Studio 2013 (Ultimate preferred)
 2.  Install Perl (keep default options checked, [link](http://www.activestate.com/activeperl/downloads))
 3. Download WinBash and add it to your PATH ([link](http://sourceforge.net/projects/win-bash/files/shell-complete/latest/))
 4. Install Java 6.0u0 32-bit ([link](http://www.oracle.com/technetwork/java/javase/downloads/java-archive-downloads-javase6-419409.html)) and add JDK\bin and JRE\bin\client to your PATH

Your PATH should now look similar to this:

`[...]C:\winbash;C:\Program Files (x86)\Java\jdk1.6.0\bin\;C:\Program Files (x86)\Java\jre1.6.0\bin\client\`

To build the server, run the `build_windows.bat` file, which will also build the game data files which you will need to run the server.

Alternately, open  `src/build/swg.sln` with Visual Studio and select the desired solution configuration (Debug/Optimized/Release) and build the `_all_server` project (`right-click project > build`).  At the moment, LoginServer only builds under Optimized, I had forgotten to change the solution configuration when adding cURL to affect all build targets.

**Additional Setup Steps**

After building the data folder using the `build_windows.bat` file, you must extract the following folders from your SWG game client into the `data/sku.0/sys.shared/compiled/game` folder, overwriting any files if asked.  You may perform this by using a tool like TRE Explorer or Sytner's IFF Editor.

 - appearance
 - object

**Server Setup Guide**

Once you have finished the above steps, you must configure your server configs.  This part should be mostly self-explanatory.  The biggest part here is to update the configs, changing `clusterName` value to reflect the cluster name you gave the DB builder from running `build_windows.bat`.

**Database Setup Guide**

You will need an Oracle Database as the SWG source relies on an ODB to store all object information and other persisted data.  We recommend installing Oracle Database 11g Express Edition due to the ease of the installation process.  ([link](http://www.oracle.com/technetwork/database/database-technologies/express-edition/overview/index.html))

Since the express edition comes with some restrictions, you will want to use a different version for large, long-lasting servers, such as the Standard or Enterprise editions.

**Optional Utilities**

 - git for windows:  It integrates shell scripting into windows very nicely, also has a simple GUI to use when commiting to a repo. ([link](https://git-for-windows.github.io/))
 - Notepad++: a step up from the Notepad utility that comes with WIndows, has syntax highlighting. ([link](https://notepad-plus-plus.org/))
 - SQLDeveloper: great utility by Oracle to access your ODB and view data (similar to Navicat).  ([link](http://www.oracle.com/technetwork/developer-tools/sql-developer/overview/index-097090.html))
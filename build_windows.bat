@echo OFF 
set basedir = %CD%
echo It is recommended that you run this build tool from the command line. "Shift + right click in this directory and click open a command prompt."
:Ask
echo Would you like to compile the server?(Y/N)
set INPUT=
set /P INPUT=Type input: %=%
If /I "%INPUT%"=="y" goto compile 
If /I "%INPUT%"=="n" goto data
echo Incorrect input & goto Ask
:compile

call "C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\vcvarsall.bat" x86
echo "Starting Build for all Projects"

cd src/build/win32
echo Starting to build All Server.
devenv "swg.sln" /build Release /project "_all_server" /projectconfig Release

echo Starting to build the LoginServer
devenv "swg.sln" /build Optimized /project "LoginServer" /projectconfig Optimized

cd ../../compile
SET COPYCMD=/Y
forfiles /S /M *.exe /C "cmd /c move @file %CD%"../../../exe/win32

cd %basedir%
copy exe\win32\cfg\*.* exe\win32\

:data
cd %basedir%
echo Would you like to compile all data? This takes alot of time. You can execute the builders single by using "utils/build_java", etc from the command prompt of swg-src(Y/N)
set INPUT=
set /P INPUT=Type input: %=%
If /I "%INPUT%"=="y" goto compile_data_all
If /I "%INPUT%"=="n" goto db
echo Incorrect input & goto data

:compile_data_all
goto compile_data_tdf
goto compile_data_tab
goto compile_data_tpf
goto compile_data_miff
goto compile_data_java

:compile_data_tdf
call "utils/build_tdf.bat"

:compile_data_tab
call "utils/build_tab.sh"

:compile_data_tpf
call "utils/build_tpf.sh"

:compile_data_miff
call "utils/build_miff.sh"

:compile_data_java
call "utils/build_java.bat"
pause

:db
echo Would you like to build the Database?(Y/N)
set INPUT=
set /P INPUT=Type input: %=%
If /I "%INPUT%"=="y" goto build_db
If /I "%INPUT%"=="n" goto pause
echo Incorrect input & goto db

:build_db
"utils/build_db.sh"

:pause
pause